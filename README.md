1. OVERVIEW
===================


The core idea behind the robocup-history project is the creation of a common, machine readable storage structure for tournaments.
This common structure is described using xml schema definitions.

I decided to use xml files as data storage because they are in my opinion the best choice for the given task.


2. RUN IT
===================
Simply open the tourament_example.xml in a browser. You should get a xsl transformed version of the xml-file, looking like a usual website.