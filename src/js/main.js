/** *******************************************************
                   Tournament JavaScript

The default JavaScript function set for tournaments.

Authors:
  * Stefan Glaser

******************************************************** */

/** The RoboCupArchive namespace declaration */
var RCA = { REVISION: '1.0' };

RCA.NO_OVERLAYS = window.location.search.indexOf( 'noOverlays' ) !== -1;
RCA.activeOverlay = null;


/** Toggle visibility of collapsable td element */
RCA.toggleCollapsableTD = function ( expandable, id ) {
  var cell = document.getElementById( id );

  var arrowDiv = expandable.getElementsByClassName( 'expandable-arrow' )[0];

  if ( cell !== undefined ) {
    if ( cell.style.display !== 'table-cell' ) {
      cell.style.display = 'table-cell';

      if ( arrowDiv !== undefined ) {
        arrowDiv.title ='Collapse';
        arrowDiv.style.transform = 'translate(-50%,50%) scale(1,-1)';
      }
    } else {
      cell.style.display = 'none';

      if ( arrowDiv !== undefined ) {
        arrowDiv.title ='Expand';
        arrowDiv.style.transform = 'translate(-50%,50%) scale(1,1)';
      }
    }
  }
};


RCA.showTeamDetails = function ( teamID ) {
  if ( RCA.NO_OVERLAYS ) {
    window.location.hash = '#team-' + teamID;
    return false;
  }

  var listDiv = document.getElementById( 'team-details-list' );
  var teamDiv = document.getElementById( 'team-' + teamID );

  // Hide all child divs of overlay div
  RCA.hideAllDivs( listDiv.childNodes );

  // Activate overlay and corresponding team page if available
  if ( teamDiv !== undefined ) {
    // Remember activated match overlay
    RCA.activeOverlay = 'team-' + teamID;

    // Activate embedded stream boxes
    RCA.activateEmbeddedBoxes( teamDiv );

    // Make Team list visible
    teamDiv.style.display = null;
    listDiv.style.display = 'block';
  }

  return true;
};

RCA.hideTeamDetails = function () {
  if ( RCA.NO_OVERLAYS ) {
    return false;
  }

  RCA.closeOverlay( 'team-details-list' );

  return true;
};


RCA.showMatchDetails = function ( matchID ) {
  if ( RCA.NO_OVERLAYS ) {
    window.location.hash = '#match-' + matchID;
    return false;
  }

  var listDiv = document.getElementById( 'match-details-list' );
  var matchDiv = document.getElementById( 'match-' + matchID );

  // Hide all child divs of overlay div
  RCA.hideAllDivs( listDiv.childNodes );

  // Activate overlay and corresponding match page if available
  if ( matchDiv !== undefined ) {
    // Remember activated match overlay
    RCA.activeOverlay = 'match-' + matchID;

    // RCA.activateOverlayIframes();
    RCA.activateEmbeddedBoxes( matchDiv );

    // Make Match list visible
    matchDiv.style.display = null;
    listDiv.style.display = 'block';
  }

  return true;
};

RCA.hideMatchDetails = function () {
  if ( RCA.NO_OVERLAYS ) {
    return false;
  }

  RCA.closeOverlay( 'match-details-list' );

  return true;
};









/** ==================== General overlay functionality ==================== START */
RCA.closeOverlay = function ( overlayID ) {
  console.log('Closing: ' + overlayID);
  document.getElementById( overlayID ).style.display = 'none';

  RCA.clearActiveOverlay();
};

RCA.clearActiveOverlay = function () {
  // Try to reset all stream iframes of the currently active overlay
  // This causes reloading of embedded videos and thus stops playing them
  var i, iframes;

  if ( RCA.activeOverlay !== null ) {
    RCA.deactivateEmbeddedBoxes( document.getElementById( RCA.activeOverlay ) );
    // iframes = document.getElementById( RCA.activeOverlay ).getElementsByTagName( 'iframe' );

    // for ( i = 0; i < iframes.length; ++i ) {
    //   if ( iframes[i].className.indexOf( 'stream-frame' ) !== -1 ) {
    //     iframes[i].src = '';
    //   }
    // }
  }

  // Clear reference to active overlay
  RCA.activeOverlay = null;
};

RCA.activateOverlayIframes = function () {
  // Try to reset all stream iframes of the currently active overlay
  // This causes reloading of embedded videos and thus stops playing them
  var i, iframes;

  if ( RCA.activeOverlay !== null ) {
    iframes = document.getElementById( RCA.activeOverlay ).getElementsByTagName( 'iframe' );

    for ( i = 0; i < iframes.length; ++i ) {
      if ( iframes[i].className.indexOf( 'stream-frame' ) !== -1 ) {
        iframes[i].src = iframes[i].parentNode.getElementsByTagName( 'select' )[0].value;
      }
    }
  }

  // Clear reference to active overlay
  RCA.activeOverlay = null;
};


RCA.transformToOverlay = function ( divElement ) {
  // Check if the given element is already an overlay
  if ( divElement === undefined || divElement.className.indexOf( 'overlay' ) !== -1 ) {
    return;
  }

  var nodes = divElement.childNodes;
  var closeBtn;
  var closeFcn = function () {
    RCA.closeOverlay( divElement.id );
  };

  // Disable onClick event on all child divs
  for ( var i = 0; i < nodes.length; ++i ) {
    if ( nodes[i].tagName === 'DIV' ) {
      nodes[i].addEventListener( 'click', RCA.disableEventHandler , false );

      closeBtn = document.createElement( 'input' );
      closeBtn.type = 'button';
      closeBtn.title = 'Close Overlay';
      closeBtn.className = 'overlayCloseBtn';
      closeBtn.value ='X';
      closeBtn.addEventListener( 'click', closeFcn, false );
      nodes[i].appendChild( closeBtn );
    }
  }

  // Add overlay class to element's classes
  divElement.className = 'overlay ' + divElement.className;
};

RCA.hideAllDivs = function ( list ) {
  for ( var i = 0; i < list.length; ++i ) {
    if ( list[i].tagName === 'DIV' && list[i].style.display !== 'none') {
      list[i].style.display = 'none';
    }
  }
};
/** ==================== General overlay functionality ==================== END */




/** Simple event handler used to stop the automatic propagation of an event */
RCA.disableEventHandler = function ( evt ) {
  evt.stopPropagation();
};




// If JavaScript overlays are enabled, hook into the document
if ( !RCA.NO_OVERLAYS ) {
  RCA.transformToOverlay(document.getElementById( 'team-details-list' ));
  RCA.transformToOverlay(document.getElementById( 'match-details-list' ));
};





RCA.filterStreamSelection = function ( videoSelect, streamSelect ) {
  console.log( 'Filter Stream Selections.' );

  if ( streamSelect === undefined ) {
    streamSelect = videoSelect.parentNode.getElementsByTagName('select')[1];
  }

  var streamOptions = streamSelect.getElementsByTagName('option');
  var selectedURL = '';

  // Select a valid option
  for ( var i = 0; i < streamOptions.length; ++i) {
    if ( videoSelect.value == streamOptions[i].label ) {
      // Enable option
      streamOptions[i].style.display = null;

      if ( selectedURL === '' ) {
        streamSelect.selectedIndex = i;
        selectedURL = streamSelect.value;
      }
    } else {
      // Disable option
      streamOptions[i].style.display = 'none';
    }
  }

  var iframe = videoSelect.parentNode.getElementsByTagName( 'iframe' )[0];

  if ( iframe !== undefined ) {
    iframe.src = selectedURL;
  }
};






RCA.activateEmbeddedBoxes = function ( element ) {
  console.log( 'Activate embedded boxes.' );

  var boxes = element.getElementsByClassName( 'embedded-box' );

  for ( var i = 0; i < boxes.length; ++i ) {
    var videoSelect = boxes[i].getElementsByClassName( 'video' )[0];
    var streamSelect = boxes[i].getElementsByClassName( 'stream' )[0];
    var frameDiv = boxes[i].getElementsByClassName( 'stream-frame' )[0];

    videoSelect.selectedIndex = 0;
    RCA.filterStreamSelection( videoSelect, streamSelect );

    frameDiv.innerHTML = '<iframe src="' + streamSelect.value + '" width="100%" height="100%" frameborder="0" allowfullscreen="allowfullscreen"></iframe>';
  }
};


RCA.deactivateEmbeddedBoxes = function ( element ) {
  console.log( 'Deactivate embedded boxes.' );

  var frameDivs = element.getElementsByClassName( 'stream-frame' );

  for ( var i = 0; i < frameDivs.length; ++i ) {
    frameDivs[i].innerHTML = ''
  }
};
