var gulp = require('gulp'),
  gulpLoadPlugins = require('gulp-load-plugins'),
  gp = gulpLoadPlugins(),
  path = require('path'),
  pump = require('pump');


var buildDir = path.join(__dirname, 'archive/app/history');
var srcDir = path.join(__dirname, 'src');


gulp.task('build', ['build-js', 'build-sass']);


gulp.task('build-js', function (cb) {
  pump([
      gulp.src(path.join(srcDir, 'js/*.js')),
      gp.concat('history.min.js'),
      gp.uglify(),
      gulp.dest(path.join(buildDir, 'js'))
    ],
    cb
  );
});

gulp.task('build-sass', function () {
  return gulp.src(path.join(srcDir, 'scss/*.scss'))
    .pipe(gp.sass({ style: 'expanded', sourceComments: 'map', errLogToConsole: true }))
    .pipe(gulp.dest(path.join(buildDir, 'css')))
    // .pipe(gp.notify({ message: 'LibSass files dropped!' }))
    ;
});

gulp.task('clean', function() {
  return gulp.src([path.join(buildDir, 'css/*'), path.join(buildDir, 'js/*')], { read: false })
   .pipe(gp.clean());
});




//  Default Gulp Task
//===========================================
gulp.task('default', ['build'], function() {

});


// Watch Files For Changes
//===========================================
gulp.task('watch', function() {
    gulp.watch(path.join(srcDir, 'js/*.js'), ['build-js']);
    gulp.watch(path.join(srcDir, 'scss/*.scss'), ['build-sass']);
});
