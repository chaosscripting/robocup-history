<?php header("Access-Control-Allow-Origin: *");
			header("Content-Type: application/json");

// Definitions
define("BASE_PATH",	"../../soccer/simulation");


// The folder/replay entry class
class Entry
{
  public $label = "";
  public $path = "";

  public static function cmpLabels($a, $b) {
	  return strcmp($a->label, $b->label);
	}
}


// Funktion used for creating an dynamic Entry object
function makeEntry($path, $fileName) {
  $lp = new Entry();
  $lp->label = $fileName;
  $lp->path = $path . $fileName;

  return $lp;
}


// Funktion used for creating an static Entry object
function staticEntry($path, $label) {
  $lp = new Entry();
  $lp->label = $label;
  $lp->path = $path;

  return $lp;
}


// Function to create a direcory listing response
function makeListing($path) {
	$folders = array();
	$replays = array();

	$dirIter = new DirectoryIterator(BASE_PATH . $path);

	if ($dirIter->valid()) {
		$folderIdx = 0;
		$replayIdx = 0;

		foreach ($dirIter as $fn) {
			if ($fn->isDir() && !$fn->isDot()) {
				$folders[$folderIdx] = makeEntry($path, $fn->getFilename());
				$folderIdx++;
			} else if (!$fn->isDot()) {
				$fileSuffixL = substr($fn->getFilename(), -7);
				$fileSuffixS = substr($fn->getFilename(), -6);

				if ($fileSuffixL == ".replay" || $fileSuffixS == ".rpl2d" || $fileSuffixS == ".rpl3d") {
					$replays[$replayIdx] = makeEntry(BASE_PATH . $path, $fn->getFilename());
					$replayIdx++;
				}
			}
		}

		// Sort entries
		usort($folders, array("Entry", "cmpLabels"));
		usort($replays, array("Entry", "cmpLabels"));

		// Create response
		return array(
				"type" => "archive",
				// "basePath" => BASE_PATH,
				"path" => strlen($path) > 1 ? substr($path, 0, -1) : $path,
		    "folders" => $folders,
		    "replays" => $replays
		);
	} else {
		return array(
				"type" => "archive",
				"errorMsg" => "Error reading directory!"
		);
	}
}



$errorMsg = "";
$valid = true;
$path = "/";


// Check for path argument
if (isset($_GET['path'])) {
	// Validate external path argument
	$splitPath = explode("/", $_GET['path']);
	$path = "/";

	foreach ($splitPath as $value) {
		if ($value == "..") {
			$valid = false;
			$errorMsg = "Invalid path argument!";
			break;
		} else if ($value != "") {
			$path = $path . $value . "/";
		}
	}
}

// Process request if valid
if ($valid) {
	if ($path == "/") {
		// Prepare static root response
		$response = array(
				"type" => "archive",
				"path" => "/",
		    "folders" => array(staticEntry("/2D", "2D Simulation"),
		    									 staticEntry("/3D", "3D Simulation")),
		    "replays" => array()
		);
	} else if ($path == "/2D/") {
		// Prepare static 2D root response
		$response = array(
				"type" => "archive",
				"path" => "/2D",
		    "folders" => array(),
		    "replays" => array()
		);
	} else if ($path == "/3D/") {
		// Prepare static 3D root response
		$response = array(
				"type" => "archive",
				"path" => "/3D",
		    "folders" => array(staticEntry("/3D/events/WorldCup/2016/replays", "RoboCup 2016")),
		    "replays" => array()
		);
	} else {
		// Check if the requested path exists
		if ($valid && !file_exists(BASE_PATH . $path)) {
			$valid = false;
			$errorMsg = "Not found!";
		}

		// Prepare dynamic response
		if ($valid) {
			$response = makeListing($path);
		}
	}
}

if (!$valid) {
	// Prepare error response
	$response = array(
			"type" => "archive",
			"errorMsg" => $errorMsg
	);
}


echo json_encode($response);

?>