<?xml version="1.0" encoding="UTF-8" ?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <!--                      Tournament XSL                      -->
  <!--                                                          -->
  <!-- The Default XML StyleSheet for tournaments.              -->
  <!--                                                          -->
  <!-- Authors:                                                 -->
  <!--  * Stefan Glaser                                         -->
  <!--                                                          -->

  <xsl:output method="html" encoding="UTF-8" indent="yes"/>
  <xsl:strip-space elements="*"/>












  <!-- ==================================================================== -->
  <!--  Keys  ============================================================= -->
  <!-- ==================================================================== -->
  <!-- <xsl:key name="team" match="/tournament/teams/team" use="id" /> -->
  <xsl:key name="matchByDate" match="//match" use="date" />












  <!-- ==================================================================== -->
  <!--  Global variables  ================================================= -->
  <!-- ==================================================================== -->
  <!-- Character constants -->
  <xsl:variable name="QUOT">"</xsl:variable>
  <xsl:variable name="APOS">'</xsl:variable>
  <xsl:variable name="smallcase" select="'abcdefghijklmnopqrstuvwxyz'" />
  <xsl:variable name="uppercase" select="'ABCDEFGHIJKLMNOPQRSTUVWXYZ'" />


  <!-- Path variables -->
  <xsl:variable name="APP_PATH">/app/history</xsl:variable>
  <xsl:variable name="IMG_PATH"><xsl:value-of select="$APP_PATH"/>/img</xsl:variable>

  <xsl:variable name="JASMIN_PATH">/app/JaSMIn</xsl:variable>

  <xsl:variable name="TOURNAMENT_PATH"><xsl:value-of select="/tournament/@path"/></xsl:variable>


  <!-- Quick reference to all teams and matches -->
  <xsl:variable name="TEAMS" select="/tournament/teams/team"/>
  <xsl:variable name="ALL_MATCHES" select="/tournament//match"/>


  <!-- Tournament scoring parameter -->
  <xsl:variable name="POINTS_PER_WIN">
    <xsl:choose>
      <xsl:when test="/tournament/params/scoring/pointsPerWin">
        <xsl:value-of select="/tournament/params/scoring/pointsPerWin"/>
      </xsl:when>
      <xsl:otherwise>3</xsl:otherwise>
    </xsl:choose>
  </xsl:variable>

  <xsl:variable name="POINTS_PER_DRAW">
    <xsl:choose>
      <xsl:when test="/tournament/params/scoring/pointsPerDraw">
        <xsl:value-of select="/tournament/params/scoring/pointsPerDraw"/>
      </xsl:when>
      <xsl:otherwise>1</xsl:otherwise>
    </xsl:choose>
  </xsl:variable>

  <xsl:variable name="LIN_OFFSET">
    <xsl:choose>
      <xsl:when test="/tournament/params/scoring/linOffset">
        <xsl:value-of select="/tournament/params/scoring/linOffset"/>
      </xsl:when>
      <xsl:otherwise>5</xsl:otherwise>
    </xsl:choose>
  </xsl:variable>

  <xsl:variable name="LIN_RANGE">
    <xsl:choose>
      <xsl:when test="/tournament/params/scoring/linRange">
        <xsl:value-of select="/tournament/params/scoring/linRange"/>
      </xsl:when>
      <xsl:otherwise>25</xsl:otherwise>
    </xsl:choose>
  </xsl:variable>

  <xsl:variable name="LIN_RANKS">
    <xsl:choose>
      <xsl:when test="/tournament/params/scoring/linRanks">
        <xsl:value-of select="/tournament/params/scoring/linRanks"/>
      </xsl:when>
      <xsl:otherwise>0</xsl:otherwise>
    </xsl:choose>
  </xsl:variable>












  <!-- ==================================================================== -->
  <!--   ROOT TEMPLATE  =================================================== -->
  <!-- ==================================================================== -->
  <xsl:template match="/">
    <xsl:apply-templates select="tournament"/>
  </xsl:template>






  <!-- ==================================================================== -->
  <!--   <tournament> Templates =========================================== -->
  <!-- ==================================================================== -->
  <xsl:template match="tournament">
    <html>
      <head>
        <title>
          <xsl:apply-templates select="event"/>
          <xsl:text> - </xsl:text>
          <xsl:value-of select="category"/>
          <xsl:text> </xsl:text>
          <xsl:value-of select="league"/>
          <xsl:text> </xsl:text>
          <xsl:value-of select="subleague"/>
        </title>
        <link rel="stylesheet" type="text/css" media="screen, print" href="{$APP_PATH}/css/history.css"/>
      </head>
      <body>
        <!-- The top-link -->
        <a id="top-link" href="#top" title="Top"></a>

        <div id="top">
          <!-- Title box -->
          <div class="title-box">
            <div class="table title">
              <div class="t-row">
                <div class="t-cell">
                  <!-- TODO: Add event image here -->
                </div>
                <div class="t-cell">
                  <h1 class="trn-title">
                    <a href="{event/@href}">
                      <xsl:apply-templates select="event"/>
                      <span class="league-title">
                        <xsl:value-of select="category"/>
                        <xsl:text> </xsl:text>
                        <xsl:value-of select="league"/>
                        <xsl:text> </xsl:text>
                        <xsl:value-of select="subleague"/>
                      </span>
                      <span class="trn-date">
                        <xsl:call-template name="fromToDateLine">
                          <xsl:with-param name="beginDate" select="event/beginDate"/>
                          <xsl:with-param name="endDate" select="event/endDate"/>
                        </xsl:call-template>
                      </span>
                    </a>
                  </h1>
                </div>
                <xsl:apply-templates select="event/venue" mode="title"/>
              </div>
            </div>
          </div>


          <!-- Info box -->
          <div class="navi-wrap">
            <div class="navi-box">
              <ul class="navigation">
                <li>
                  <a href="#competitions">Competitions</a>
                  <ul>
                    <xsl:for-each select="competition">
                      <li><a href="#comp-{id}"><xsl:value-of select="name"/></a></li>
                    </xsl:for-each>
                  </ul>
                </li>

                <li>
                  <a href="#challenges">Challenges</a>
                  <ul>
                    <xsl:for-each select="challenge">
                      <li><a href="#chal-{id}"><xsl:value-of select="name"/></a></li>
                    </xsl:for-each>
                  </ul>
                </li>

                <li>
                  <a href="#awards">Awards</a>
                  <ul>
                    <xsl:for-each select="award">
                      <li><a href="#awd-{id}"><xsl:value-of select="name"/></a></li>
                    </xsl:for-each>
                  </ul>
                </li>

                <li>
                  <a href="#appendix">Appendix</a>
                  <ul>
                    <li><a href="#all-matches-list">List of all matches</a></li>
                    <li><a href="#footer">Event Information</a></li>
                  </ul>
                </li>
              </ul>
            </div>
          </div>


          <!-- Print list of all teams -->
          <div id="teams">
            <xsl:apply-templates select="teams"/>
          </div>
          <div class="section-spacer"/>


          <!-- Print all competitions -->
          <xsl:if test="competition">
            <div id="competitions">
              <xsl:apply-templates select="competition"/>
            </div>
          </xsl:if>
          <div class="section-spacer"/>


          <!-- Print all challenges -->
          <xsl:if test="challenge">
            <div id="challenges">
              <xsl:apply-templates select="challenge"/>
            </div>
          </xsl:if>
          <div class="section-spacer"/>


          <!-- Print all awards -->
          <xsl:if test="award">
            <div id="awards">
              <xsl:apply-templates select="award"/>
            </div>
          </xsl:if>
          <div class="section-spacer"/>


          <!-- Appendix box (details of teams, matches, etc.) -->
          <div id="appendix">
            <!-- List all matches grouped by date -->
            <div id="all-matches-list" class="content-box">
              <h1>List of All Matches</h1>
              <xsl:for-each select="$ALL_MATCHES[generate-id() = generate-id(key('matchByDate', date)[1])]">
                <xsl:sort select="date" order="ascending"/>

                <h3>
                  <xsl:apply-templates select="date" mode="literal"/>
                </h3>

                <table class="matches-list main-table">
                  <xsl:apply-templates select="key('matchByDate', date)" mode="table-row">
                    <xsl:sort select="time" order="ascending"/>
                  </xsl:apply-templates>
                </table>
              </xsl:for-each>
            </div>

            <!-- Print team details list -->
            <xsl:apply-templates select="teams" mode="details-list"/>

            <!-- Print match details list -->
            <div id="match-details-list" class="match-details-box" onClick="RCA.hideMatchDetails()">
              <xsl:apply-templates select="$ALL_MATCHES" mode="details">
                <xsl:sort select="concat(date, time)" order="ascending"/>
              </xsl:apply-templates>
            </div>
          </div>
          <div class="section-spacer"/>


          <!-- Footer box -->
          <div id="footer">
            <div class="content-box">
              <table class="tournament-details">
                <tr>
                  <td rowspan="5" class="oc">
                    <span>Organizing Committee:</span>
                    <ul>
                      <xsl:apply-templates select="oc/person" mode="item"/>
                    </ul>
                    <span>Local Organizing Committee:</span>
                    <ul>
                      <xsl:apply-templates select="loc/person" mode="item"/>
                    </ul>
                  </td>

                  <td rowspan="5" class="spacer"></td>

                  <td class="label">Event:</td>

                  <td class="event">
                    <xsl:apply-templates select="event" mode="link"/>
                  </td>
                </tr>

                <tr>
                  <td class="label">Promoter:</td>
                  <td class="promoter">
                    <xsl:apply-templates select="event/promoter"/>
                  </td>
                </tr>

                <tr>
                  <td class="label">League:</td>
                  <td class="league">
                    <xsl:apply-templates select="league">
                      <xsl:with-param name="prefix" select="concat(category, ' ')"/>
                    </xsl:apply-templates>
                  </td>
                </tr>

                <tr>
                  <td class="label">Sub-League:</td>
                  <td class="sub-league">
                    <xsl:apply-templates select="subleague"/>
                  </td>
                </tr>

                <tr>
                  <td class="label">Rules:</td>
                  <td class="rules">
                    <xsl:apply-templates select="rules"/>
                  </td>
                </tr>
              </table>
            </div>
          </div>
        </div>
      </body>
      <script src="{$APP_PATH}/js/history.min.js"/>
    </html>
  </xsl:template>
























  <!-- ==================================================================== -->
  <!--  <abstract>  Templates ============================================= -->
  <!-- ==================================================================== -->
  <xsl:template match="abstract">
    <h6>Abstract:</h6>
    <p class="abstract"><xsl:value-of select="."/></p>
  </xsl:template>






  <!-- ==================================================================== -->
  <!--  <address>  Templates ============================================== -->
  <!-- ==================================================================== -->
  <xsl:template match="address">
    <ul class="address">
      <li><xsl:value-of select="line1"/></li>
      <li><xsl:value-of select="line2"/></li>
      <li><xsl:value-of select="line3"/></li>
      <li><xsl:value-of select="city"/></li>
      <li><xsl:value-of select="region"/></li>
      <li><xsl:value-of select="country"/></li>
    </ul>
  </xsl:template>






  <!-- ==================================================================== -->
  <!--  <affiliation>  Templates ========================================== -->
  <!-- ==================================================================== -->
  <xsl:template match="affiliation">
    <xsl:choose>
      <xsl:when test="@href">
        <a href="{@href}" target="_blank"><xsl:value-of select="."/></a>
      </xsl:when>
      <xsl:otherwise><xsl:value-of select="."/></xsl:otherwise>
    </xsl:choose>
  </xsl:template>



  <xsl:template match="affiliation" mode="item">
    <li><xsl:apply-templates select="."/></li>
  </xsl:template>






  <!-- ==================================================================== -->
  <!--  <affiliations>  Templates ========================================= -->
  <!-- ==================================================================== -->
  <xsl:template match="affiliations">
    <xsl:for-each select="affiliation">
      <xsl:if test="position() &gt; 1"><xsl:text>, </xsl:text></xsl:if>
      <xsl:apply-templates select="."/>
    </xsl:for-each>
  </xsl:template>



  <!-- List of affiliations -->
  <xsl:template match="affiliations" mode="list">
    <ul>
      <xsl:apply-templates select="affiliation" mode="item"/>
    </ul>
  </xsl:template>






  <!-- ==================================================================== -->
  <!--  <author>  Templates ============================================== -->
  <!-- ==================================================================== -->
  <xsl:template match="author">
    <xsl:value-of select="."/>
  </xsl:template>






  <!-- ==================================================================== -->
  <!--  <authors>  Templates ============================================== -->
  <!-- ==================================================================== -->
  <xsl:template match="authors">
    <span class="by-line">
      <xsl:text>by: </xsl:text>
      <xsl:for-each select="author">
        <xsl:if test="position() &gt; 1"><xsl:text>, </xsl:text></xsl:if>
        <xsl:value-of select="."/>
      </xsl:for-each>
    </span>
  </xsl:template>






  <!-- ==================================================================== -->
  <!--  <award>  Templates ================================================ -->
  <!-- ==================================================================== -->
  <xsl:template match="award">
    <xsl:if test="position() &gt; 0">
      <div class="subsection-spacer"/>
    </xsl:if>

    <div id="awd-{id}" class="content-box">
      <h1><xsl:value-of select="name"/></h1>

      <xsl:apply-templates select="ranking" mode="simple"/>
    </div>
  </xsl:template>






  <!-- ==================================================================== -->
  <!--  <category>  Templates ============================================= -->
  <!-- ==================================================================== -->
  <xsl:template match="category[@href]">
    <a href="{@href}"><xsl:value-of select="."/></a>
  </xsl:template>






  <!-- ==================================================================== -->
  <!--  <challenge>  Templates ============================================ -->
  <!-- ==================================================================== -->
  <xsl:template match="challenge">
    <xsl:if test="position() &gt; 0">
      <div class="subsection-spacer"/>
    </xsl:if>

    <div id="chal-{id}" class="content-box">
      <h1><xsl:value-of select="name"/></h1>

      <!-- Navigation ?!? -->

      <!-- Print ranking stairs -->
      <xsl:apply-templates select="ranking"/>

      <!-- Print individual subchallenges -->
      <xsl:apply-templates select="subchallenge"/>

      <!-- Print direct challenge -->
      <xsl:apply-templates select="simple"/>
    </div>
  </xsl:template>






  <!-- ==================================================================== -->
  <!--  <competition>  Templates ========================================== -->
  <!-- ==================================================================== -->
  <xsl:template match="competition">
    <xsl:variable name="pointsPerWin">
      <xsl:choose>
        <xsl:when test="scoring/pointsPerWin"><xsl:value-of select="scoring/pointsPerWin"/></xsl:when>
        <xsl:otherwise><xsl:value-of select="$POINTS_PER_WIN"/></xsl:otherwise>
      </xsl:choose>
    </xsl:variable>

    <xsl:variable name="pointsPerDraw">
      <xsl:choose>
        <xsl:when test="scoring/pointsPerDraw"><xsl:value-of select="scoring/pointsPerDraw"/></xsl:when>
        <xsl:otherwise><xsl:value-of select="$POINTS_PER_DRAW"/></xsl:otherwise>
      </xsl:choose>
    </xsl:variable>


    <xsl:if test="position() &gt; 0">
      <div class="subsection-spacer"/>
    </xsl:if>

    <div id="comp-{id}" class="content-box">
      <h1><xsl:value-of select="name"/></h1>

      <!-- Navigation ?!? -->

      <!-- Print ranking stairs -->
      <xsl:apply-templates select="ranking"/>

      <!-- Print Rounds -->
      <xsl:apply-templates select="round">
        <xsl:with-param name="idPrefix" select="id"/>
        <xsl:with-param name="pointsPerWin" select="$pointsPerWin"/>
        <xsl:with-param name="pointsPerDraw" select="$pointsPerDraw"/>
      </xsl:apply-templates>
    </div>
  </xsl:template>






  <!-- ==================================================================== -->
  <!--  <countryCode>  Templates ========================================== -->
  <!-- ==================================================================== -->
  <xsl:template match="countryCode">
    <img src="{$IMG_PATH}/countries/{.}.png" alt="{.}" title="{.}" class="country-icon"/>
  </xsl:template>






  <!-- ==================================================================== -->
  <!--  <date>  Templates ================================================= -->
  <!-- ==================================================================== -->
  <xsl:template match="date">
    <!-- YYYY.MM.DD -->
    <!-- Month -->
    <xsl:value-of select="substring(., 6, 2)"/>
    <xsl:text>/</xsl:text>
    <!-- Day -->
    <xsl:value-of select="substring(., 9, 2)"/>
    <xsl:text>/</xsl:text>
    <!-- Year -->
    <xsl:value-of select="substring(., 1, 4)"/>
  </xsl:template>



  <!-- Date with short month name -->
  <xsl:template match="date" mode="literal">
    <!-- YYYY.MM.DD -->
    <!-- Day -->
    <xsl:value-of select="number(substring(., 9, 2))"/>
    <xsl:text> </xsl:text>
    <!-- Month -->
    <xsl:call-template name="printMonthName">
      <xsl:with-param name="month" select="number(substring(., 6, 2))"/>
    </xsl:call-template>
    <xsl:text> </xsl:text>
    <!-- Year -->
    <xsl:value-of select="substring(., 1, 4)"/>
  </xsl:template>



  <!-- Date with short month name -->
  <xsl:template match="date" mode="literal-short">
    <!-- YYYY.MM.DD -->
    <!-- Day -->
    <xsl:value-of select="number(substring(., 9, 2))"/>
    <xsl:text> </xsl:text>
    <!-- Month -->
    <xsl:call-template name="printMonthShortName">
      <xsl:with-param name="month" select="number(substring(., 6, 2))"/>
    </xsl:call-template>
    <xsl:text> </xsl:text>
    <!-- Year -->
    <xsl:value-of select="substring(., 1, 4)"/>
  </xsl:template>






  <!-- ==================================================================== -->
  <!--  <event>  Templates ================================================ -->
  <!-- ==================================================================== -->
  <xsl:template match="event">
    <xsl:value-of select="name"/>
    <xsl:text> </xsl:text>
    <xsl:value-of select="year"/>
  </xsl:template>



  <!-- Event Link -->
  <xsl:template match="event" mode="link">
    <xsl:choose>
      <xsl:when test="@href">
        <a href="{@href}">
          <xsl:apply-templates select="."/>
        </a>
      </xsl:when>
      <xsl:otherwise>
        <xsl:apply-templates select="."/>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>






  <!-- ==================================================================== -->
  <!--  <evaluation>  Templates =========================================== -->
  <!-- ==================================================================== -->
  <xsl:template match="evaluation" mode="table-head">
    <th class="evaluation"><abbr title="{title}"><xsl:value-of select="title/@abbr"/></abbr></th>
  </xsl:template>






  <!-- ==================================================================== -->
  <!--  <file>  Templates ================================================= -->
  <!-- ==================================================================== -->
  <xsl:template match="file[@path]">
    <a href="{@path}"><xsl:value-of select="."/></a>
  </xsl:template>



  <!-- A file item -->
  <xsl:template match="file[@path]" mode="item">
    <li><xsl:apply-templates select="."/></li>
  </xsl:template>






  <!-- ==================================================================== -->
  <!--  <goal>  Templates ================================================= -->
  <!-- ==================================================================== -->
  <xsl:template match="goal" mode="item">
    <xsl:param name="own-team"/>
    <xsl:param name="opponent-team"/>

    <li>
      <xsl:choose>
        <xsl:when test="@ownGoal">
          <span title="Own Goal">*</span>
          <xsl:apply-templates select="$opponent-team/players/player[id = current()]"/>
        </xsl:when>
        <xsl:otherwise>
          <xsl:apply-templates select="$own-team/players/player[id = current()]"/>
        </xsl:otherwise>
      </xsl:choose>
      <xsl:text>&#160;</xsl:text>
      <span title="Time: {substring(@time, 4, 5)}">
        <xsl:value-of select="format-number(number(substring(@time, 4, 2)) + 1,'#0')"/>
        <xsl:value-of select="$APOS"/>
      </span>
    </li>
  </xsl:template>






  <!-- ==================================================================== -->
  <!--  <goals>  Templates ================================================ -->
  <!-- ==================================================================== -->
  <xsl:template match="goals" mode="list">
    <xsl:param name="own-team"/>
    <xsl:param name="opponent-team"/>

    <br/>
    <ul class="goal-list">
      <xsl:apply-templates select="goal" mode="item">
        <xsl:with-param name="own-team" select="$own-team"/>
        <xsl:with-param name="opponent-team" select="$opponent-team"/>
      </xsl:apply-templates>
    </ul>
  </xsl:template>






  <!-- ==================================================================== -->
  <!--  <group>  Templates ================================================ -->
  <!-- ==================================================================== -->
  <xsl:template match="group">
    <xsl:param name="idPrefix"/>
    <xsl:param name="pointsPerWin"/>
    <xsl:param name="pointsPerDraw"/>

    <xsl:variable name="gid" select="concat($idPrefix, '-G', id)"/>
    <xsl:variable name="groupMatches" select="matches/match"/>


    <h4 id="{$gid}"><xsl:value-of select="name"/></h4>

    <table class="group-ranking main-table noOuterPadding">
      <tr>
        <th class="rank">Rank</th>
        <th class="team">Team</th>
        <th class="games">Games</th>

        <th class="spacer"></th>
        <th class="won">Won</th>
        <th class="draw">Draw</th>
        <th class="lost">Lost</th>

        <th class="spacer"></th>
        <th class="goals-for"><abbr title="Goals For">GF</abbr></th>
        <th class="goals-against"><abbr title="Goals Against">GA</abbr></th>
        <th class="goal-diff"><abbr title="Goal Difference">+/-</abbr></th>

        <th class="spacer"></th>
        <th class="points">Points</th>
      </tr>


      <!-- List each team in ascending order -->
      <xsl:for-each select="ranking/rank">
        <xsl:sort select="@no" order="ascending"/>

        <xsl:variable name="tid"><xsl:value-of select="."/></xsl:variable>

        <xsl:variable name="allMatches" select="$groupMatches[left/tid = $tid or right/tid = $tid]"/>
        <xsl:variable name="normalMatches" select="$allMatches[not(@decider)]"/>
        <xsl:variable name="deciderMatches" select="$allMatches[@decider]"/>

        <xsl:variable name="numNormalGames" select="count($normalMatches)"/>
        <xsl:variable name="numDeciderGames" select="count($deciderMatches)"/>

        <xsl:variable name="numWonNormal" select="count($normalMatches[left/tid = $tid and
                    count(left/goals/goal) + count(left/penalties/penalty[@success]) &gt; count(right/goals/goal) + count(right/penalties/penalty[@success])])
                                                + count($normalMatches[right/tid = $tid and
                    count(right/goals/goal) + count(right/penalties/penalty[@success]) &gt; count(left/goals/goal)]) + count(left/penalties/penalty[@success])"/>
        <xsl:variable name="numWonDecider" select="count($deciderMatches[left/tid = $tid and
                    count(left/goals/goal) + count(left/penalties/penalty[@success]) &gt; count(right/goals/goal) + count(right/penalties/penalty[@success])])
                                                 + count($deciderMatches[right/tid = $tid and
                    count(right/goals/goal) + count(right/penalties/penalty[@success]) &gt; count(left/goals/goal) + count(left/penalties/penalty[@success])])"/>

        <xsl:variable name="numDrawNormal" select="count($normalMatches[left/tid = $tid and
                    count(left/goals/goal) + count(left/penalties/penalty[@success]) = count(right/goals/goal) + count(right/penalties/penalty[@success])])
                                                 + count($normalMatches[right/tid = $tid and
                    count(right/goals/goal) + count(right/penalties/penalty[@success]) = count(left/goals/goal) + count(left/penalties/penalty[@success])])"/>
        <xsl:variable name="numDrawDecider" select="count($deciderMatches[left/tid = $tid and
                    count(left/goals/goal) + count(left/penalties/penalty[@success]) = count(right/goals/goal) + count(right/penalties/penalty[@success])])
                                                  + count($deciderMatches[right/tid = $tid and
                    count(right/goals/goal) + count(right/penalties/penalty[@success]) = count(left/goals/goal) + count(left/penalties/penalty[@success])])"/>



        <xsl:variable name="numGoalsForNormal" select="count($normalMatches/left[tid = $tid]/goals/goal) + count($normalMatches/right[tid = $tid]/goals/goal)"/>
        <xsl:variable name="numGoalsForDecider" select="count($deciderMatches/left[tid = $tid]/goals/goal) + count($deciderMatches/right[tid = $tid]/goals/goal)"/>

        <xsl:variable name="numGoalsAgainstNormal" select="count($normalMatches[left/tid = $tid]/right/goals/goal)
                                                         + count($normalMatches[right/tid = $tid]/left/goals/goal)"/>
        <xsl:variable name="numGoalsAgainstDecider" select="count($deciderMatches[left/tid = $tid]/right/goals/goal)
                                                          + count($deciderMatches[right/tid = $tid]/left/goals/goal)"/>

        <tr class="team-row expandable" onClick="RCA.toggleCollapsableTD(this, 'collaps-{generate-id(current())}')">
          <td class="rank">
            <div class="expandable-arrow" title="Expand"/>
            <span><xsl:value-of select="@no"/></span>
          </td>

          <td class="team">
            <xsl:apply-templates select="/tournament/teams/team[id = $tid]" mode="details-link"/>
          </td>

          <td class="games">
            <xsl:value-of select="$numNormalGames"/>
            <xsl:if test="$numDeciderGames &gt; 0">
              <br/><small><xsl:value-of select="$numDeciderGames"/></small>
            </xsl:if>
          </td>

          <td class="spacer"></td>

          <td class="won">
            <xsl:value-of select="$numWonNormal"/>
            <xsl:if test="$numDeciderGames &gt; 0">
              <br/><small><xsl:value-of select="$numWonDecider"/></small>
            </xsl:if>
          </td>

          <td class="draw">
            <xsl:value-of select="$numDrawNormal"/>
            <xsl:if test="$numDeciderGames &gt; 0">
              <br/><small><xsl:value-of select="$numDrawDecider"/></small>
            </xsl:if>
          </td>

          <td class="lost">
            <xsl:value-of select="$numNormalGames - $numWonNormal - $numDrawNormal"/>
            <xsl:if test="$numDeciderGames &gt; 0">
              <br/><small><xsl:value-of select="$numDeciderGames - $numWonDecider - $numDrawDecider"/></small>
            </xsl:if>
          </td>

          <td class="spacer"></td>

          <td class="goals-for">
            <xsl:value-of select="$numGoalsForNormal"/>
            <xsl:if test="$numDeciderGames &gt; 0">
              <br/><small><xsl:value-of select="$numGoalsForDecider"/></small>
            </xsl:if>
          </td>

          <td class="goals-against">
            <xsl:value-of select="$numGoalsAgainstNormal"/>
            <xsl:if test="$numDeciderGames &gt; 0">
              <br/><small><xsl:value-of select="$numGoalsAgainstDecider"/></small>
            </xsl:if>
          </td>

          <td class="goal-diff">
            <xsl:value-of select="$numGoalsForNormal - $numGoalsAgainstNormal"/>
            <xsl:if test="$numDeciderGames &gt; 0">
              <br/><small><xsl:value-of select="$numGoalsForDecider - $numGoalsAgainstDecider"/></small>
            </xsl:if>
          </td>

          <td class="spacer"></td>

          <td class="points">
            <xsl:value-of select="$numWonNormal * $pointsPerWin + $numDrawNormal * $pointsPerDraw"/>
            <xsl:if test="$numDeciderGames &gt; 0">
              <br/><small><xsl:value-of select="$numWonDecider * $pointsPerWin + $numDrawDecider * $pointsPerDraw"/></small>
            </xsl:if>
          </td>
        </tr>

        <tr>
          <td id="collaps-{generate-id(.)}" colspan="13" class="collapsable">
            <table class="matches-sublist matches-list main-table">
              <xsl:apply-templates select="$allMatches" mode="table-row">
                <xsl:sort select="concat(date, time)" order="ascending"/>
              </xsl:apply-templates>
            </table>
          </td>
        </tr>
      </xsl:for-each>
    </table>
  </xsl:template>






  <!-- ==================================================================== -->
  <!--  <league>  Templates =============================================== -->
  <!-- ==================================================================== -->
  <xsl:template match="league[not(@href)]">
    <xsl:param name="prefix" select="''"/>

    <xsl:value-of select="$prefix"/>
    <xsl:value-of select="."/>
  </xsl:template>



  <!-- League tag with href -->
  <xsl:template match="league[@href]">
    <xsl:param name="prefix" select="''"/>

    <a href="{@href}">
      <xsl:value-of select="$prefix"/>
      <xsl:value-of select="."/>
    </a>
  </xsl:template>






  <!-- ==================================================================== -->
  <!--  <log>  Templates ================================================== -->
  <!-- ==================================================================== -->
  <xsl:template match="log[@path]">
    <a href="{@path}">
      <xsl:attribute name="download">
        <xsl:call-template name="extractFileName">
          <xsl:with-param name="path" select="@path"/>
        </xsl:call-template>
      </xsl:attribute>
      <xsl:value-of select="."/>
    </a>
  </xsl:template>



  <!-- A logfile item -->
  <xsl:template match="log[@path]" mode="item">
    <li><xsl:apply-templates select="."/></li>
  </xsl:template>






  <!-- ==================================================================== -->
  <!--  <match>  Templates ================================================ -->
  <!-- ==================================================================== -->
  <xsl:template match="match" mode="score">
    <xsl:variable name="leftScore" select="count(left/goals/goal)"/>
    <xsl:variable name="rightScore" select="count(right/goals/goal)"/>

    <span class="score">
      <xsl:value-of select="$leftScore"/>
      <xsl:text>-</xsl:text>
      <xsl:value-of select="$rightScore"/>
    </span>
    <xsl:if test="left/penalties | right/penalties">
      <br/><xsl:text>&#160;</xsl:text>
      <span class="penalties">
        <xsl:text>(</xsl:text>
        <xsl:value-of select="$leftScore + count(left/penalties/penalty[@success])"/>
        <xsl:text>-</xsl:text>
        <xsl:value-of select="$rightScore + count(right/penalties/penalty[@success])"/>
        <xsl:text>) after penalties</xsl:text>
      </span>
    </xsl:if>
  </xsl:template>



  <!-- Match table row -->
  <xsl:template match="match" mode="table-row">
    <xsl:param name="roundTitle" select="''"/>

    <!-- <xsl:variable name="matchID" select="concat($idPrefix, '-M', id)"/> -->
    <xsl:variable name="matchID" select="generate-id(.)"/>

    <tr class="match-row details-link" onClick="RCA.showMatchDetails('{$matchID}')">
      <td class="info">
        <span class="date-time-line">
          <xsl:apply-templates select="date" mode="literal-short"/>
          <xsl:apply-templates select="time" mode="date-suffix"/>
        </span>

        <xsl:if test="$roundTitle != ''">
          <br/>
          <span class="round"><xsl:value-of select="$roundTitle"/></span>
        </xsl:if>

        <xsl:if test="location">
          <br/>
          <span class="location"><xsl:value-of select="location"/></span>
        </xsl:if>

        <xsl:if test="@decider">
          <br/>
          <span class="decider">Decider match</span>
        </xsl:if>
      </td>

      <td class="left-team">
        <xsl:apply-templates select="$TEAMS[id = current()/left/tid]" mode="details-link"/>
      </td>

      <td class="score">
        <xsl:apply-templates select="." mode="score"/>
      </td>

      <td class="right-team">
        <xsl:apply-templates select="$TEAMS[id = current()/right/tid]" mode="details-link"/>
      </td>
    </tr>
  </xsl:template>



  <!-- Match details -->
  <xsl:template match="match" mode="details">
    <xsl:param name="roundTitle" select="''"/>

    <xsl:variable name="matchID" select="generate-id(.)"/>
    <xsl:variable name="leftTeam" select="$TEAMS[id = current()/left/tid]"/>
    <xsl:variable name="rightTeam" select="$TEAMS[id = current()/right/tid]"/>

    <!-- Create a team details page -->
    <div id="match-{$matchID}" class="match-details">
      <div class="scrollableInY">
        <table class="match-title-table main-table">
          <tr>
            <th colspan="3">
              <span class="date-time-line">
                <xsl:apply-templates select="date" mode="literal"/>
                <xsl:apply-templates select="time" mode="date-suffix"/>
              </span>
              <br/>
              <span class="title">
                <xsl:if test="local-name(../../.) = 'group'">
                  <xsl:value-of select="../../../../name"/>
                  <xsl:text> - </xsl:text>
                </xsl:if>
                <xsl:value-of select="../../../name"/>
                <xsl:text> - </xsl:text>
                <xsl:value-of select="../../name"/>
              </span>
            </th>
          </tr>

          <tr>
            <td class="left-team">
              <xsl:apply-templates select="$leftTeam" mode="title"/>
              <xsl:apply-templates select="left/goals" mode="list">
                <xsl:with-param name="own-team" select="$leftTeam"/>
                <xsl:with-param name="opponent-team" select="$rightTeam"/>
              </xsl:apply-templates>
            </td>

            <td class="score">
              <xsl:apply-templates select="." mode="score"/>
            </td>

            <td class="right-team">
              <xsl:apply-templates select="$rightTeam" mode="title"/>
              <xsl:apply-templates select="right/goals" mode="list">
                <xsl:with-param name="own-team" select="$rightTeam"/>
                <xsl:with-param name="opponent-team" select="$leftTeam"/>
              </xsl:apply-templates>
            </td>
          </tr>
        </table>



        <div class="inner-overlay-content">
          <xsl:apply-templates select="resources"/>
          <xsl:if test="not(resources)">
            <div class="notfound">No match resources available.</div>
          </xsl:if>

          <h3>Lineups</h3>
          <xsl:apply-templates select="." mode="lineup-table"/>
        </div>
      </div>
    </div>
  </xsl:template>



  <!-- Match Lineup table -->
  <xsl:template match="match" mode="lineup-table">
    <xsl:variable name="leftPlayers" select="$TEAMS[id = current()/left/tid]/players/player[id = current()/left/lineup/player[not(@backup)]]"/>
    <xsl:variable name="leftSubPlayers" select="$TEAMS[id = current()/left/tid]/players/player[id = current()/left/lineup/player[@backup]]"/>
    <xsl:variable name="rightPlayers" select="$TEAMS[id = current()/right/tid]/players/player[id = current()/right/lineup/player[not(@backup)]]"/>
    <xsl:variable name="rightSubPlayers" select="$TEAMS[id = current()/right/tid]/players/player[id = current()/right/lineup/player[@backup]]"/>

    <xsl:variable name="mostPlayers" select="$leftPlayers | $rightPlayers[position() &gt; count($leftPlayers)]"/>
    <xsl:variable name="mostSubPlayers" select="$leftSubPlayers | $rightSubPlayers[position() &gt; count($leftSubPlayers)]"/>

    <table class="lineup-table match-lineup-table main-table">
      <tr>
        <th colspan="3" class="lineup-header">
          <xsl:apply-templates select="$TEAMS[id = current()/left/tid]"/>
        </th>

        <th colspan="3" class="lineup-header">
          <xsl:apply-templates select="$TEAMS[id = current()/right/tid]"/>
        </th>
      </tr>

      <!-- Initial Lineup -->
      <xsl:for-each select="$mostPlayers">
        <xsl:variable name="pos" select="position()"/>
        <xsl:variable name="left" select="$leftPlayers[position() = $pos]"/>
        <xsl:variable name="right" select="$rightPlayers[position() = $pos]"/>

        <tr class="player-row">
          <td class="number">
            <xsl:value-of select="$left/no"/>
          </td>
          <td class="name">
            <xsl:value-of select="$left/name"/>
          </td>
          <td class="events">
            <xsl:text>events</xsl:text>
          </td>

          <td class="number">
            <xsl:value-of select="$right/no"/>
          </td>
          <td class="name">
            <xsl:value-of select="$right/name"/>
          </td>
          <td class="events">
            <xsl:text>events</xsl:text>
          </td>
        </tr>
      </xsl:for-each>

      <xsl:if test="count($mostSubPlayers) &gt; 0">
        <tr>
          <th colspan="6" class="substitude">Substitudes</th>
        </tr>


        <xsl:for-each select="$mostSubPlayers">
          <xsl:variable name="pos" select="position()"/>
          <xsl:variable name="left" select="$leftSubPlayers[position() = $pos]"/>
          <xsl:variable name="right" select="$rightSubPlayers[position() = $pos]"/>

          <tr class="player-row">
            <td class="number">
              <xsl:value-of select="$left/no"/>
            </td>
            <td class="name">
              <xsl:value-of select="$left/name"/>
            </td>
            <td class="events">
              <xsl:text>events</xsl:text>
            </td>

            <td class="number">
              <xsl:value-of select="$right/no"/>
            </td>
            <td class="name">
              <xsl:value-of select="$right/name"/>
            </td>
            <td class="events">
              <xsl:text>events</xsl:text>
            </td>
          </tr>
        </xsl:for-each>
      </xsl:if>
    </table>
  </xsl:template>






  <!-- ==================================================================== -->
  <!--  <member>  Templates =============================================== -->
  <!-- ==================================================================== -->
  <xsl:template match="member">
    <xsl:choose>
      <xsl:when test="email">
        <a href="mailto:{email}" class="mail"><xsl:value-of select="name"/></a>
      </xsl:when>
      <xsl:when test="web">
        <a href="{web}"><xsl:value-of select="name"/></a>
      </xsl:when>
      <xsl:otherwise><xsl:value-of select="name"/></xsl:otherwise>
    </xsl:choose>
  </xsl:template>



  <!-- Member item -->
  <xsl:template match="member" mode="item">
    <li><xsl:apply-templates select="."/></li>
  </xsl:template>






  <!-- ==================================================================== -->
  <!--  <paper>  Templates ================================================ -->
  <!-- ==================================================================== -->
  <xsl:template match="paper">
    <xsl:choose>
      <xsl:when test="@path">
        <a href="{@path}"><xsl:value-of select="title"/></a>
      </xsl:when>
      <xsl:otherwise><xsl:value-of select="title"/></xsl:otherwise>
    </xsl:choose>
  </xsl:template>



  <xsl:template match="paper" mode="full">
    <div class="paper-entry">
      <h5><xsl:apply-templates select="."/></h5>
      <xsl:apply-templates select="authors"/>
      <xsl:apply-templates select="abstract"/>
    </div>
  </xsl:template>






  <!-- ==================================================================== -->
  <!--  <person>  Templates =============================================== -->
  <!-- ==================================================================== -->
  <xsl:template match="person">
    <xsl:choose>
      <xsl:when test="web">
        <a href="{web}"><xsl:value-of select="name"/></a>
      </xsl:when>
      <xsl:when test="email">
        <a href="mailto:{email}"><xsl:value-of select="name"/></a>
      </xsl:when>
      <xsl:otherwise><xsl:value-of select="name"/></xsl:otherwise>
    </xsl:choose>
  </xsl:template>



  <!-- A person item -->
  <xsl:template match="person" mode="item">
    <li><xsl:apply-templates select="."/></li>
  </xsl:template>






  <!-- ==================================================================== -->
  <!--  <player>  Templates =============================================== -->
  <!-- ==================================================================== -->
  <xsl:template match="player">
    <xsl:value-of select="name"/>
  </xsl:template>



  <!-- Player lineup cells -->
  <xsl:template match="player" mode="lineup-cells">
    <td class="player-no">
      <xsl:value-of select="no"/>
    </td>

    <td class="player-name">
      <xsl:value-of select="name"/>
    </td>
  </xsl:template>






  <!-- ==================================================================== -->
  <!--  <promoter>  Templates ============================================= -->
  <!-- ==================================================================== -->
  <xsl:template match="promoter">
    <xsl:choose>
      <xsl:when test="@href">
        <a href="{@href}"><xsl:value-of select="."/></a>
      </xsl:when>
      <xsl:otherwise><xsl:value-of select="."/></xsl:otherwise>
    </xsl:choose>
  </xsl:template>






  <!-- ==================================================================== -->
  <!--  <qualification>  Templates ======================================== -->
  <!-- ==================================================================== -->
  <xsl:template match="qualification">
    <table class="resource-subtable">
      <xsl:if test="tdp">
        <tr class="resource-row">
          <td class="label"><abbr title="Team Description Paper">TDP</abbr>:</td>
          <td class="resource"><xsl:apply-templates select="tdp" mode="file-name"/></td>
        </tr>
      </xsl:if>

      <xsl:if test="specifications">
        <tr class="resource-row">
          <td class="label">Specs:</td>
          <td class="resource"><xsl:apply-templates select="specifications"/></td>
        </tr>
      </xsl:if>

      <xsl:if test="videos">
        <tr class="resource-row">
          <td class="label">Videos:</td>
          <td class="resource"><xsl:apply-templates select="videos"/></td>
        </tr>
      </xsl:if>
    </table>
  </xsl:template>



  <!-- Detailed qualification table for team details -->
  <xsl:template match="qualification" mode="details">
    <h3>Qualification</h3>
    <table class="qualification-table main-table">
      <xsl:if test="tdp">
        <tr>
          <td class="label"><abbr title="Team Description Paper">TDP</abbr>:</td>
          <td><xsl:apply-templates select="tdp" mode="full"/></td>
        </tr>
      </xsl:if>

      <xsl:if test="videos">
        <tr>
          <td class="label">Videos:</td>
          <td>
            <xsl:apply-templates select="videos" mode="embedded-box"/>
            <xsl:apply-templates select="videos" mode="list"/>
          </td>
        </tr>
      </xsl:if>

      <xsl:if test="specifications">
        <tr>
          <td class="label">Specs:</td>
          <td><xsl:apply-templates select="specifications" mode="list"/></td>
        </tr>
      </xsl:if>
    </table>
  </xsl:template>






  <!-- ==================================================================== -->
  <!--  <rank>  Templates ================================================= -->
  <!-- ==================================================================== -->
  <xsl:template match="rank">
    <xsl:apply-templates select="$TEAMS[id = current()]" mode="details-link"/>
  </xsl:template>



  <!-- A numbered rank -->
  <xsl:template match="rank" mode="numbered">
    <div class="rank">
      <xsl:apply-templates select="." mode="rank"/>
      <xsl:apply-templates select="."/>
    </div>
  </xsl:template>



  <!-- The rank itself (1st, 2nd, etc.) -->
  <xsl:template match="rank" mode="rank">
    <span class="rank">
      <xsl:value-of select="@no"/>
      <small>
        <xsl:choose>
          <xsl:when test="@no = 1">st</xsl:when>
          <xsl:when test="@no = 2">nd</xsl:when>
          <xsl:when test="@no = 3">rd</xsl:when>
          <xsl:otherwise>th</xsl:otherwise>
        </xsl:choose>
      </small>
    </span>
  </xsl:template>



  <!-- The rank itself (1st, 2nd, etc.) -->
  <xsl:template match="rank" mode="table-row">
    <tr class="team-row">
      <td class="rank">
        <xsl:apply-templates select="." mode="rank"/>
      </td>

      <td class="team">
        <xsl:apply-templates select="."/>
      </td>
    </tr>
  </xsl:template>






  <!-- ==================================================================== -->
  <!--  <ranking>  Templates ============================================== -->
  <!-- ==================================================================== -->
  <xsl:template match="ranking">
    <xsl:variable name="numTeams" select="count(rank)"/>

    <!-- Check if there exists a ranking and at least one team rank -->
    <xsl:if test="$numTeams &gt; 0">
      <div class="ranking">
        <table class="stairs">

          <!-- Trophy/Medal images -->
          <tr>
            <td rowspan="2" class="medal">
              <xsl:if test="$numTeams &gt; 1">
                <img src="{$IMG_PATH}/trophies/silver.png" width="150" />
              </xsl:if>
            </td>
            <td class="medal"><img src="{$IMG_PATH}/trophies/gold.png" width="150" /></td>
            <td rowspan="3" class="medal">
              <xsl:if test="$numTeams &gt; 2">
                <img src="{$IMG_PATH}/trophies/bronze.png" width="150" />
              </xsl:if>
            </td>
          </tr>

          <!-- Rank 1: GOLD -->
          <tr>
            <td class="team">
              <xsl:for-each select="rank[@no = 1]">
                <xsl:if test="position() &gt; 1"><br/></xsl:if>
                <xsl:apply-templates select="."/>
              </xsl:for-each>
            </td>
          </tr>

          <!-- Rank 2: SILVER and 1st Place label-->
          <tr>
            <td class="team">
              <xsl:for-each select="rank[@no = 2]">
                <xsl:if test="position() &gt; 1"><br/></xsl:if>
                <xsl:apply-templates select="."/>
              </xsl:for-each>
            </td>
            <td rowspan="3" class="solid">1<small>st</small></td>
          </tr>

          <!-- Rank 3: BRONZE and 2nd Place label-->
          <tr>
            <td rowspan="2" class="solid">2<small>nd</small></td>
            <td class="team">
              <xsl:for-each select="rank[@no = 3]">
                <xsl:if test="position() &gt; 1"><br/></xsl:if>
                <xsl:apply-templates select="."/>
              </xsl:for-each>
            </td>
          </tr>

          <!-- 3rd Place label -->
          <tr>
            <td class="solid">3<small>rd</small></td>
          </tr>

          <!-- 4th to last Places -->
          <tr>
            <td colspan="3" class="ranking">
              <div class="ranking-wrapper">
                <xsl:apply-templates select="rank[@no &gt; 3]" mode="numbered">
                  <xsl:sort select="@no" order="ascending"/>
                </xsl:apply-templates>
              </div>
            </td>
          </tr>
        </table>
      </div>
    </xsl:if>
  </xsl:template>



  <!-- Simple ranking without stairs -->
  <xsl:template match="ranking" mode="simple">
    <table class="ranking">
      <xsl:apply-templates select="rank" mode="table-row">
        <xsl:sort select="@no" order="ascending"/>
      </xsl:apply-templates>
    </table>
  </xsl:template>






  <!-- ==================================================================== -->
  <!--  <replay>  Templates =============================================== -->
  <!-- ==================================================================== -->
  <xsl:template match="replay" mode="option">
    <option value="{concat('r', position())}"><xsl:value-of select="."/></option>
  </xsl:template>



  <!-- A replay stream option -->
  <xsl:template match="replay" mode="stream-options">
    <option value="{$JASMIN_PATH}/embedded.html?replay=../../{$TOURNAMENT_PATH}/{@path}" label="{concat('r', position())}">Archive</option>
  </xsl:template>



  <!-- A replay item -->
  <xsl:template match="replay" mode="item">
    <li>
      <a href="{@path}">
        <xsl:attribute name="download">
          <xsl:call-template name="extractFileName">
            <xsl:with-param name="path" select="@path"/>
          </xsl:call-template>
        </xsl:attribute>
        <xsl:value-of select="."/>
      </a>
    </li>
  </xsl:template>






  <!-- ==================================================================== -->
  <!--  <replays>  Templates ============================================== -->
  <!-- ==================================================================== -->
  <xsl:template match="replays" mode="selection">
    <select name="replay" onchange="this.parentNode.getElementsByTagName('iframe')[0].src = this.value;">
      <xsl:apply-templates select="replay" mode="option"/>
    </select>
    <div class="stream-frame" width="560" height="315"/>
  </xsl:template>






  <!-- ==================================================================== -->
  <!--  <resources>  Templates ============================================ -->
  <!-- ==================================================================== -->
  <xsl:template match="resources">
    <table class="match-resources main-table">
      <tr>
        <th>Videos / Replays</th>
        <th>Log Files</th>
      </tr>

      <tr>
        <td class="players" rowspan="5">
          <xsl:apply-templates select="." mode="embedded-box"/>
        </td>

        <td class="files">
          <ul>
            <xsl:apply-templates select="log" mode="item"/>
            <xsl:if test="not(log)">
              <li>n/a</li>
            </xsl:if>
          </ul>
        </td>
      </tr>

      <tr>
        <th>Replay Files</th>
      </tr>

      <tr>
        <td class="files">
          <ul>
            <xsl:apply-templates select="replay" mode="item"/>
            <xsl:if test="not(replay)">
              <li>n/a</li>
            </xsl:if>
          </ul>
        </td>
      </tr>

      <tr>
        <th>Video Files / Streams</th>
      </tr>

      <tr>
        <td class="files">
          <ul>
            <xsl:apply-templates select="videos" mode="list"/>
            <xsl:if test="not(videos)">
              <li>n/a</li>
            </xsl:if>
          </ul>
        </td>
      </tr>
    </table>
  </xsl:template>



  <!-- The videos and replays in one embedded streaming box -->
  <xsl:template match="resources" mode="embedded-box">
    <xsl:variable name="vids" select="videos/video[@path or stream/@embedded]"/>

    <xsl:if test="not(replay) and not($vids)">
      <span class="notfound">n/a</span>
    </xsl:if>

    <div class="embedded-box">
      <select name="video" class="video" onchange="RCA.filterStreamSelection(this)">
        <xsl:if test="replay">
          <optgroup label="Replays">
            <xsl:apply-templates select="replay" mode="option"/>
          </optgroup>
        </xsl:if>
        <xsl:if test="$vids">
          <optgroup label="Videos">
            <xsl:apply-templates select="$vids" mode="option"/>
          </optgroup>
        </xsl:if>
      </select>
      <select name="stream" class="stream" onchange="this.parentNode.getElementsByTagName('iframe')[0].src = this.value;">
        <xsl:apply-templates select="replay" mode="stream-options"/>
        <xsl:apply-templates select="$vids" mode="stream-options"/>
      </select>
      <div class="stream-frame" width="640" height="360" />
    </div>
  </xsl:template>






  <!-- ==================================================================== -->
  <!--  <round>  Templates ================================================ -->
  <!-- ==================================================================== -->
  <xsl:template match="round">
    <xsl:param name="idPrefix"/>
    <xsl:param name="pointsPerWin"/>
    <xsl:param name="pointsPerDraw"/>

    <xsl:variable name="rid" select="concat($idPrefix, '-', id)"/>


    <h2 id="{$rid}"><xsl:value-of select="name"/></h2>

    <!-- Print groups... -->
    <xsl:apply-templates select="group">
      <xsl:with-param name="idPrefix" select="$rid"/>
      <xsl:with-param name="pointsPerWin" select="$pointsPerWin"/>
      <xsl:with-param name="pointsPerDraw" select="$pointsPerDraw"/>
    </xsl:apply-templates>

    <!-- ...or matches -->
    <xsl:if test="matches">
      <table class="matches-list main-table">
        <xsl:apply-templates select="matches/match" mode="table-row">
          <xsl:sort select="concat(date, time)" order="ascending"/>
        </xsl:apply-templates>
      </table>
    </xsl:if>
  </xsl:template>






  <!-- ==================================================================== -->
  <!--  <rules>  Templates ================================================ -->
  <!-- ==================================================================== -->
  <xsl:template match="rules">
    <ul>
      <xsl:apply-templates select="file" mode="item"/>
    </ul>
  </xsl:template>






  <!-- ==================================================================== -->
  <!--  <run>  Templates ================================================== -->
  <!-- ==================================================================== -->
  <xsl:template match="evaluation/run" mode="table-row">
    <tr class="evaluation-row hover">
      <td class="info">
        <span class="date-time-line">
          <xsl:apply-templates select="date" mode="literal"/>
          <xsl:apply-templates select="time" mode="date-suffix"/>
        </span>

        <xsl:if test="location">
          <br/>
          <span class="location"><xsl:value-of select="location"/></span>
        </xsl:if>

        <xsl:if test="@ignore | ../@ignore">
          <br/>
          <span class="ignore" title="This Evaluation doesn't contribute to the final result">Marked Ignore</span>
        </xsl:if>
      </td>

      <td class="title">
        <xsl:value-of select="../title"/>
      </td>

      <td class="score">
        <span>
          <xsl:if test="@ignore or ../@ignore">
            <xsl:attribute name="class">ignore</xsl:attribute>
            <xsl:attribute name="title">This evaluation doesn't contribute to the final result</xsl:attribute>
          </xsl:if>
          <xsl:value-of select="score"/>
          <xsl:choose>
            <xsl:when test="../unit">
              <span class="unit" title="{../unit}">&#160;<xsl:value-of select="../unit/@short"/></span>
            </xsl:when>
            <xsl:otherwise>
              <span class="unit" title="Points">&#160;pts</span>
            </xsl:otherwise>
          </xsl:choose>
        </span>
      </td>

      <td class="resources">
        <table class="resource-subtable">
          <xsl:if test="count(resources/log) &gt; 0">
            <tr class="resource-row">
              <td class="label">Logs:</td>
              <td class="resource">
                <xsl:for-each select="resources/log">
                  <xsl:if test="position() &gt; 1"><xsl:text>, </xsl:text></xsl:if>
                  <a href="{@path}"><xsl:value-of select="."/></a>
                </xsl:for-each>
              </td>
            </tr>
          </xsl:if>

          <xsl:if test="count(resources/replay) &gt; 0">
            <tr class="resource-row">
              <td class="label">Replays:</td>
              <td class="resource">
                <xsl:for-each select="resources/replay">
                  <xsl:if test="position() &gt; 1"><xsl:text>, </xsl:text></xsl:if>
                  <a href="{@path}"><xsl:value-of select="."/></a>
                </xsl:for-each>
              </td>
            </tr>
          </xsl:if>

          <xsl:if test="resources/videos">
            <tr class="resource-row">
              <td class="label">Videos:</td>
              <td class="resource">
                <xsl:for-each select="resources/videos/video">
                  <a href="{@path}"><xsl:value-of select="title"/></a>
                </xsl:for-each>
              </td>
            </tr>
          </xsl:if>
        </table>
      </td>
    </tr>
  </xsl:template>



  <!-- Evaluation run result as table cell -->
  <xsl:template match="evaluation/run" mode="result-table">
    <span class="evaluation-run">
      <xsl:if test="@ignore or ../@ignore">
        <xsl:attribute name="class">evaluation-run ignore</xsl:attribute>
        <xsl:attribute name="title">This evaluation doesn't contribute to the final result</xsl:attribute>
      </xsl:if>
      <xsl:value-of select="score"/>
      <xsl:if test="../unit">
        <span class="unit" title="{../unit}"><xsl:value-of select="../unit/@short"/></span>
      </xsl:if>
    </span>
  </xsl:template>






  <!-- ==================================================================== -->
  <!--  <simple>  Templates =============================================== -->
  <!-- ==================================================================== -->
  <xsl:template match="simple">
    <xsl:variable name="numEvals" select="count(evaluations/evaluation)"/>

    <xsl:variable name="numTeams" select="count(ranking/rank[not(@disqualified) and not(@withdrawn)])"/>
    <xsl:variable name="showTotal">
      <xsl:choose>
        <xsl:when test="@hideTotal">0</xsl:when>
        <xsl:otherwise>1</xsl:otherwise>
      </xsl:choose>
    </xsl:variable>
    <xsl:variable name="showAvg">
      <xsl:choose>
        <xsl:when test="@hideAvg">0</xsl:when>
        <xsl:otherwise>1</xsl:otherwise>
      </xsl:choose>
    </xsl:variable>
    <xsl:variable name="showRunDetails">
      <xsl:choose>
        <xsl:when test="@hideRunDetails">0</xsl:when>
        <xsl:when test="count(evaluations/evaluation) &gt; 0">1</xsl:when>
        <xsl:otherwise>0</xsl:otherwise>
      </xsl:choose>
    </xsl:variable>

    <!-- Fetch scoring offset from subchallenge, its parent challenge, or the tournament if present (in that order) -->
    <xsl:variable name="sOffset">
      <xsl:choose>
        <xsl:when test="scoring/linOffset">
          <xsl:value-of select="scoring/linOffset"/>
        </xsl:when>
        <xsl:otherwise><xsl:value-of select="$LIN_OFFSET"/></xsl:otherwise>
      </xsl:choose>
    </xsl:variable>

    <!-- Fetch scoring range from subchallenge, its parent challenge, or the tournament if present (in that order) -->
    <xsl:variable name="sRange">
      <xsl:choose>
        <xsl:when test="scoring/linRange">
          <xsl:value-of select="scoring/linRange"/>
        </xsl:when>
        <xsl:otherwise><xsl:value-of select="$LIN_RANGE"/></xsl:otherwise>
      </xsl:choose>
    </xsl:variable>

    <!-- Fetch scoring limit from subchallenge, its parent challenge, or the tournament if present (in that order) -->
    <xsl:variable name="sLimit">
      <xsl:choose>
        <xsl:when test="scoring/linRanks">
          <xsl:choose>
            <xsl:when test="scoring/linRanks &gt; 0 and scoring/linRanks &lt; $numTeams">
              <xsl:value-of select="scoring/linRanks"/>
            </xsl:when>
            <xsl:otherwise><xsl:value-of select="$numTeams"/></xsl:otherwise>
          </xsl:choose>
        </xsl:when>
        <xsl:otherwise>
          <xsl:choose>
            <xsl:when test="$LIN_RANKS &gt; 0 and $LIN_RANKS &lt; $numTeams">
              <xsl:value-of select="$LIN_RANKS"/>
            </xsl:when>
            <xsl:otherwise><xsl:value-of select="$numTeams"/></xsl:otherwise>
          </xsl:choose>
        </xsl:otherwise>
      </xsl:choose>
    </xsl:variable>


    <table class="subchallenge-ranking main-table noOuterPadding">
      <!-- Ranking header -->
      <tr>
        <th class="rank">Rank</th>

        <th class="team">Team</th>

        <xsl:if test="entries">
          <th class="papers">Entry</th>
        </xsl:if>

        <th class="spacer"></th>

        <xsl:apply-templates select="evaluations/evaluation" mode="table-head"/>

        <th class="spacer"></th>
        <xsl:if test="not(@hideTotal)">
          <th class="total">Total</th>
        </xsl:if>

        <xsl:if test="not(@hideAvg)">
          <th class="avg"><abbr title="Average Score">AVG</abbr></th>
        </xsl:if>
        <th class="points">Points</th>
      </tr>

      <!-- Print a result row for each participating team -->
      <xsl:for-each select="ranking/rank">
        <xsl:sort select="@no" order="ascending"/>

        <xsl:variable name="nodeID" select="generate-id(current())"/>
        <xsl:variable name="tid"><xsl:value-of select="."/></xsl:variable>
        <xsl:variable name="total" select="sum(../../evaluations/evaluation[not(@ignore)]/run[tid = $tid and not(@ignore)]/score)"/>
        <xsl:variable name="cnt" select="count(../../evaluations/evaluation[not(@ignore)]/run[tid = $tid and not(@ignore)])"/>
        <xsl:variable name="paperEntries" select="../../entries/paper[tid = $tid]"/>

        <tr>
          <xsl:attribute name="class">
            <xsl:text>team-row hover</xsl:text>

            <xsl:if test="count($paperEntries) + $showRunDetails &gt; 0">
              <xsl:text> expandable</xsl:text>
            </xsl:if>

            <xsl:if test="@disqualified">
              <xsl:text> disqualified</xsl:text>
            </xsl:if>

            <xsl:if test="@withdrawn">
              <xsl:text> withdrawn</xsl:text>
            </xsl:if>
          </xsl:attribute>

          <xsl:if test="count($paperEntries) + $showRunDetails &gt; 0">
            <xsl:attribute name="onClick">
              <xsl:text>RCA.toggleCollapsableTD(this, 'collaps-</xsl:text>
              <xsl:value-of select="$nodeID"/>
              <xsl:text>')</xsl:text>
            </xsl:attribute>
          </xsl:if>

          <td class="rank">
            <div class="expandable-arrow" title="Expand"/>
            <span>
              <xsl:value-of select="@no"/>
            </span>
          </td>

          <td class="team">
            <xsl:apply-templates select="$TEAMS[id = $tid]" mode="details-link"/>
          </td>

          <xsl:if test="../../entries">
            <td class="entry">
              <xsl:for-each select="../../entries/*[tid = current() and @path]">
                <xsl:if test="position() &gt; 1"><xsl:text>, </xsl:text></xsl:if>
                <a href="{@path}" title="{title}">
                  <xsl:call-template name="extractFileEnding">
                    <xsl:with-param name="path" select="@path"/>
                  </xsl:call-template>
                </a>
              </xsl:for-each>
            </td>
          </xsl:if>

          <td class="spacer"></td>

          <xsl:for-each select="../../evaluations/evaluation">
            <xsl:variable name="runs" select="run[tid = $tid]"/>

            <td class="evaluation">
              <xsl:apply-templates select="$runs" mode="result-table"/>
              <xsl:if test="not($runs)"><span class="notfound">n/a</span></xsl:if>
            </td>
          </xsl:for-each>

          <td class="spacer"></td>

          <xsl:if test="$showTotal &gt; 0">
            <td class="total">
              <xsl:choose>
                <xsl:when test="$cnt &gt; 0">
                  <xsl:value-of select="format-number($total,'##0.0')"/>
                </xsl:when>
                <xsl:otherwise><span class="notfound">n/a</span></xsl:otherwise>
              </xsl:choose>
            </td>
          </xsl:if>

          <xsl:if test="$showAvg &gt; 0">
            <td class="avg">
              <xsl:choose>
                <xsl:when test="$cnt &gt; 0">
                  <xsl:value-of select="format-number($total div $cnt,'##0.0')"/>
                </xsl:when>
                <xsl:otherwise><span class="notfound">n/a</span></xsl:otherwise>
              </xsl:choose>
            </td>
          </xsl:if>

          <td class="points">
            <xsl:choose>
              <xsl:when test="@disqualified or @withdrawn">
                <!-- The current team got disqualified or has withdrawn from the challenge, so don't display any result -->
                <span>
                  <xsl:choose>
                    <xsl:when test="@disqualified">
                      <xsl:attribute name="title">Disqualified</xsl:attribute>
                    </xsl:when>

                    <xsl:when test="@withdrawn">
                      <xsl:attribute name="title">Withdrawn</xsl:attribute>
                    </xsl:when>
                  </xsl:choose>
                  <xsl:text>- *</xsl:text>
                </span>
              </xsl:when>

              <xsl:when test="../@explicit">
                <!-- There is a marking for explicit ranking, so try reading points from points attribute -->
                <xsl:choose>
                  <xsl:when test="@points">
                    <xsl:value-of select="format-number(@points,'##0.0')"/>
                  </xsl:when>
                  <xsl:otherwise><xsl:text>0</xsl:text></xsl:otherwise>
                </xsl:choose>
              </xsl:when>

              <xsl:when test="@no &gt; 0 and @no &lt; $sLimit + 1">
                <!-- The current team is part of a dynamic scoring schemata based on its rank -->
                <xsl:value-of select="format-number($sOffset + $sRange * ($sLimit - @no) div ($sLimit - 1),'##0.0')"/>
              </xsl:when>

              <!-- By default give no points -->
              <xsl:otherwise><xsl:text>0</xsl:text></xsl:otherwise>
            </xsl:choose>
          </td>
        </tr>


        <!-- Print run and or paper details collapsable -->
        <xsl:if test="count($paperEntries) + $showRunDetails &gt; 0">
          <tr>
            <td id="collaps-{$nodeID}" colspan="{6 + $showTotal + $showAvg + $numEvals}" class="collapsable">

              <!-- Print paper details -->
              <xsl:apply-templates select="$paperEntries" mode="full"/>

              <!-- Print run details -->
              <xsl:if test="$showRunDetails &gt; 0">
                <table class="evaluations-sublist evaluations-list main-table">
                  <xsl:apply-templates select="../../evaluations/evaluation/run[tid = $tid]" mode="table-row"/>
                </table>
              </xsl:if>
            </td>
          </tr>
        </xsl:if>
      </xsl:for-each>
    </table>
  </xsl:template>






  <!-- ==================================================================== -->
  <!--  <subleague>  Templates ============================================ -->
  <!-- ==================================================================== -->
  <xsl:template match="subleague[not(@href)]">
    <xsl:value-of select="."/>
  </xsl:template>



  <xsl:template match="subleague[@href]">
    <a href="{@href}"><xsl:value-of select="."/></a>
  </xsl:template>






  <!-- ==================================================================== -->
  <!--  <subchallenge>  Templates ========================================= -->
  <!-- ==================================================================== -->
  <xsl:template match="subchallenge">
    <h2><xsl:value-of select="name"/></h2>

    <div class="subchallenge-box">
      <xsl:apply-templates select="simple"/>
    </div>
  </xsl:template>






  <!-- ==================================================================== -->
  <!--  <tdp>  Templates ================================================== -->
  <!-- ==================================================================== -->
  <xsl:template match="tdp">
    <a href="{@path}"><xsl:value-of select="title"/></a>
  </xsl:template>



  <!-- A tdp link using the file name -->
  <xsl:template match="tdp" mode="file-name">
    <xsl:variable name="fielName">
      <xsl:call-template name="extractFileName">
        <xsl:with-param name="path" select="@path"/>
      </xsl:call-template>
    </xsl:variable>

    <xsl:choose>
      <xsl:when test="@path">
        <a href="{@path}"><xsl:value-of select="$fielName"/></a>
      </xsl:when>
      <xsl:otherwise><xsl:value-of select="$fielName"/></xsl:otherwise>
    </xsl:choose>
  </xsl:template>



  <!-- Detailed Team Description Paper -->
  <xsl:template match="tdp" mode="full">
    <div class="paper-entry">
      <h5><xsl:apply-templates select="."/></h5>
      <xsl:apply-templates select="authors"/>
      <xsl:apply-templates select="abstract"/>
    </div>
  </xsl:template>






  <!-- ==================================================================== -->
  <!--  <team>  Templates ================================================= -->
  <!-- ==================================================================== -->
  <xsl:template match="team">
    <span class="team-span"><xsl:value-of select="name"/></span>
  </xsl:template>



  <!-- Simple team title with icon -->
  <xsl:template match="team[not(@href)]" mode="title">
    <div class="team-name-box">
      <span class="team-title">
        <xsl:apply-templates select="countryCode"/>
        <xsl:value-of select="name"/>
      </span>
    </div>
  </xsl:template>



  <!-- Team title with icon and web-link -->
  <xsl:template match="team[@href]" mode="title">
    <div class="team-name-box">
      <a href="{@href}" class="team-title">
        <xsl:apply-templates select="countryCode"/>
        <xsl:value-of select="name"/>
      </a>
    </div>
  </xsl:template>



  <!-- Team details link title with icon -->
  <xsl:template match="team" mode="details-link">
    <div class="team-name-box team-details-link"
         onClick="event.stopPropagation();RCA.showTeamDetails('{id}');"
         title="Show details for {name}">
      <span class="team-title">
        <xsl:apply-templates select="countryCode"/>
        <xsl:value-of select="name"/>
      </span>
    </div>
  </xsl:template>



  <!-- Team list row -->
  <xsl:template match="team" mode="table-row">
    <tr class="team-row hover">
      <td class="team">
        <xsl:apply-templates select="." mode="details-link"/>
      </td>

      <td class="contact">
        <ul>
          <xsl:apply-templates select="members/member[@contact]" mode="item"/>
        </ul>
      </td>

      <td class="qualification">
        <xsl:apply-templates select="qualification"/>
      </td>

      <td class="status">
        <span><xsl:value-of select="status"/></span>
        <xsl:choose>
          <xsl:when test="count(members/member) &gt; 0">
            <span>&#160;(<xsl:value-of select="count(members/member[not(notRegistered)])"/>)</span>
          </xsl:when>
          <xsl:otherwise><span title="No team member information available.">&#160;(n/a)</span></xsl:otherwise>
        </xsl:choose>
      </td>
    </tr>
  </xsl:template>



  <!-- Team list row -->
  <xsl:template match="team" mode="details">
    <div id="team-{id}" class="team-details">
      <div class="scrollableInY">
        <table class="team-title-table main-table">
          <tr>
            <th>&#160;<br/>Team Details</th>
          </tr>

          <tr>
            <td class="team-name">
              <xsl:apply-templates select="." mode="title"/>
            </td>
          </tr>
        </table>

        <div class="inner-overlay-content">
          <h3>Profile</h3>
          <div class="table w100">
            <div class ="t-row">
              <div class ="t-cell w50">
                <table class="team-profile-table main-table">
                  <tr>
                    <td class="label">Leader:</td>
                    <td class="leader">
                      <ul>
                        <xsl:apply-templates select="members/member[@leader]" mode="item"/>
                        <xsl:if test="not(members/member[@leader])"><li class="notfound">n/a</li></xsl:if>
                      </ul>
                    </td>
                  </tr>

                  <tr>
                    <td class="label">Contact:</td>
                    <td class="contact">
                      <ul>
                        <xsl:apply-templates select="members/member[@contact]" mode="item"/>
                        <xsl:if test="not(members/member[@contact])"><li class="notfound">n/a</li></xsl:if>
                      </ul>
                    </td>
                  </tr>

                  <tr>
                    <td class="label">Members:</td>
                    <td class="members">
                      <ul>
                        <xsl:apply-templates select="members/member[not(@leader)]" mode="item"/>
                      </ul>
                    </td>
                  </tr>
                </table>
              </div>

              <div class="t-cell w50">
                <table class="team-profile-table main-table">
                  <tr>
                    <td class="label">Website:</td>
                    <td class="website">
                      <xsl:choose>
                        <xsl:when test="@href">
                          <a href="{@href}"><xsl:value-of select="@href"/></a>
                        </xsl:when>
                        <xsl:otherwise><span class="notfound">n/a</span></xsl:otherwise>
                      </xsl:choose>
                    </td>
                  </tr>

                  <tr>
                    <td class="label">Affiliations:</td>
                    <td class="affiliations">
                      <ul>
                        <xsl:apply-templates select="affiliations/affiliation" mode="item"/>
                        <xsl:if test="not(affiliations)"><li class="notfound">n/a</li></xsl:if>
                      </ul>
                    </td>
                  </tr>

                  <tr>
                    <td class="label">Address:</td>
                    <td class="address">
                      <xsl:apply-templates select="address"/>
                    </td>
                  </tr>
                </table>
              </div>
            </div>
          </div>


          <!-- Qualification material -->
          <xsl:apply-templates select="qualification" mode="details"/>


          <!-- Player lineup -->
          <xsl:if test="players">
            <xsl:variable name="tid" select="id"/>

            <h3>Players / Lineup</h3>
            <table class="lineup-table team-lineup-table main-table">
              <tr>
                <th class="name">Name</th>
                <th class="number"><abbr title="Player Number">num</abbr></th>
                <th class="robot">Robot</th>
                <th class="appearances"><abbr title="Appearances">APP</abbr></th>
                <th class="appearances"><abbr title="Substitude Player Appearances">SUB</abbr></th>
              </tr>

              <xsl:for-each select="players/player">
                <xsl:variable name="playerOccurences" select="$ALL_MATCHES/*[(self::left or self::right) and tid = $tid]/lineup/player[. = current()/id]"/>
                <xsl:variable name="numAppearances" select="count($playerOccurences)"/>
                <xsl:variable name="numBackup" select="count($playerOccurences[@backup])"/>

                <tr class="player-row hover">
                  <td class="name"><xsl:value-of select="name"/></td>
                  <td class="number"><xsl:value-of select="no"/></td>
                  <td class="robot"><xsl:value-of select="robot"/></td>
                  <td class="appearances">
                    <xsl:copy-of select="$numAppearances - $numBackup"/>
                  </td>
                  <td class="appearances">
                    <xsl:copy-of select="$numBackup"/>
                  </td>
                </tr>
              </xsl:for-each>
            </table>
          </xsl:if>
        </div>
      </div>
    </div>
  </xsl:template>






  <!-- ==================================================================== -->
  <!--  <teams>  Templates ================================================ -->
  <!-- ==================================================================== -->
  <xsl:template match="teams">
    <div class="content-box">
      <h1>List of Teams</h1>

      <table class="team-table main-table noOuterPadding">
        <tr>
          <th class="team">Team</th>
          <th class="contact">Contact</th>
          <th class="qualification">Qualification Material</th>
          <th class="status">Status&#160;(<abbr title="Number of registered members">#</abbr>)</th>
        </tr>

        <xsl:apply-templates select="team" mode="table-row">
          <xsl:sort select="name" order="ascending"/>
        </xsl:apply-templates>
      </table>
    </div>
  </xsl:template>



  <xsl:template match="teams" mode="details-list">
    <div id="team-details-list" class="team-details-box" onClick="RCA.hideTeamDetails()">
      <xsl:apply-templates select="team" mode="details">
        <xsl:sort select="name" order="ascending"/>
      </xsl:apply-templates>
    </div>
  </xsl:template>






  <!-- ==================================================================== -->
  <!--  <time>  Templates ================================================= -->
  <!-- ==================================================================== -->
  <xsl:template match="time">
    <xsl:value-of select="substring(., 1, 5)"/>
  </xsl:template>



  <!-- Match-row time line -->
  <xsl:template match="time" mode="date-suffix">
    <xsl:text> - </xsl:text>
    <xsl:value-of select="substring(., 1, 5)"/>
    <xsl:text>&#160;</xsl:text>
    <abbr title="Local Time">LT</abbr>
  </xsl:template>






  <!-- ==================================================================== -->
  <!--  <ŝpecifications>  Templates ======================================= -->
  <!-- ==================================================================== -->
  <xsl:template match="specifications">
    <xsl:for-each select="specification">
      <xsl:if test="position() &gt; 1"><xsl:text>, </xsl:text></xsl:if>
      <a href="{@path}"><xsl:value-of select="."/></a>
    </xsl:for-each>
  </xsl:template>



  <!-- Specification list -->
  <xsl:template match="specifications" mode="list">
    <ul class="specifications">
      <xsl:for-each select="specification">
        <li><a href="{@path}"><xsl:value-of select="."/></a></li>
      </xsl:for-each>
    </ul>
  </xsl:template>






  <!-- ==================================================================== -->
  <!--  <Stream>  Templates =============================================== -->
  <!-- ==================================================================== -->
  <xsl:template match="stream">

  </xsl:template>



  <!-- A Stream short link -->
  <xsl:template match="stream" mode="short-link">
    <xsl:text> </xsl:text>
    <a href="{@href}" class="stream-link">
      <img src="{$IMG_PATH}/{translate(., $uppercase, $smallcase)}.svg" title="Watch on {.}" alt="({.})"/>
    </a>
  </xsl:template>



  <!-- A Stream option -->
  <xsl:template match="stream" mode="option">
    <option value="{@embedded}"><xsl:value-of select="."/></option>
  </xsl:template>






  <!-- ==================================================================== -->
  <!--  <venue>  Templates ================================================ -->
  <!-- ==================================================================== -->
  <xsl:template match="venue" mode="title">
    <div class="t-cell venue">
      <span class="venue-name">
        <xsl:value-of select="name"/>
      </span>
      <br/>
      <span class="venue-address">
        <xsl:value-of select="address/line1"/>
        <br/>
        <xsl:value-of select="address/city"/>
        <xsl:text> - </xsl:text>
        <xsl:value-of select="address/country"/>
      </span>
    </div>
  </xsl:template>






  <!-- ==================================================================== -->
  <!--  <video>  Templates ================================================ -->
  <!-- ==================================================================== -->
  <xsl:template match="video">
    <xsl:choose>
      <xsl:when test="@path">
        <a href="{@path}"><xsl:value-of select="title"/></a>
      </xsl:when>
      <xsl:otherwise>
        <xsl:value-of select="title"/>
      </xsl:otherwise>
    </xsl:choose>

    <xsl:for-each select="stream">
      <xsl:text> </xsl:text>
      <a href="{@href}" class="stream-link">
        <img src="{$IMG_PATH}/{.}.png" alt="({.})"/>
      </a>
    </xsl:for-each>
  </xsl:template>



  <!-- A video item -->
  <xsl:template match="video" mode="item">
    <li>
      <xsl:choose>
        <xsl:when test="@path">
          <a href="{@path}">
            <xsl:attribute name="download">
              <xsl:call-template name="extractFileName">
                <xsl:with-param name="path" select="@path"/>
              </xsl:call-template>
            </xsl:attribute>
            <xsl:value-of select="title"/>
          </a>
        </xsl:when>
        <xsl:otherwise>
          <span><xsl:value-of select="title"/></span>
        </xsl:otherwise>
      </xsl:choose>

      <xsl:apply-templates select="stream" mode="short-link"/>
    </li>
  </xsl:template>



  <!-- A video option -->
  <xsl:template match="video" mode="option">
    <option value="{concat('v', position())}"><xsl:value-of select="title"/></option>
  </xsl:template>



  <!-- A video stream option -->
  <xsl:template match="video" mode="stream-options">
    <xsl:variable name="id" select="concat('v', position())"/>

    <xsl:if test="@path">
      <option value="{$APP_PATH}/embedded_video.html?path=../../{$TOURNAMENT_PATH}/{@path}" label="{$id}">Archive</option>
    </xsl:if>

    <xsl:for-each select="stream[@embedded]">
      <option value="{@embedded}" label="{$id}"><xsl:value-of select="."/></option>
    </xsl:for-each>
  </xsl:template>






  <!-- ==================================================================== -->
  <!--  <videos>  Templates =============================================== -->
  <!-- ==================================================================== -->
  <xsl:template match="videos">
    <xsl:for-each select="video">
      <xsl:if test="position() &gt; 1"><xsl:text>, </xsl:text></xsl:if>
      <xsl:apply-templates select="."/>
    </xsl:for-each>
  </xsl:template>



  <!-- A list of videos -->
  <xsl:template match="videos" mode="list">
    <ul class="videos">
      <xsl:apply-templates select="video" mode="item"/>
    </ul>
  </xsl:template>



  <!-- An embedded-box of videos -->
  <xsl:template match="videos" mode="embedded-box">
    <div class="embedded-box">
      <select name="video" class="video" onchange="RCA.filterStreamSelection(this)">
        <xsl:apply-templates select="video" mode="option"/>
      </select>
      <select name="stream" class="stream" onchange="this.parentNode.getElementsByTagName('iframe')[0].src = this.value;">
        <xsl:apply-templates select="video" mode="stream-options"/>
      </select>
      <div class="stream-frame" width="640" height="360" />
    </div>
  </xsl:template>




































  <!-- ==================================================================== -->
  <!-- ==================================================================== -->
  <!-- ========================  NAMED  TEMPLATES  ======================== -->
  <!-- ==================================================================== -->
  <!-- ==================================================================== -->


  <!-- Create a from-to-date line.
    Examples:
    -Same dates: "11 March 2017"
    -Different days in same month and year: "2 - 6 April 2018"
    -Different days and months in same year: "28 March - 6 April 2018"
    -Different days, months and years: "28 December 2017 - 6 January 2018"
  -->
  <xsl:template name="fromToDateLine">
    <xsl:param name="beginDate"/>
    <xsl:param name="endDate"/>

    <xsl:variable name="bYear" select="number(substring($beginDate, 1, 4))"/>
    <xsl:variable name="bMonth" select="number(substring($beginDate, 6, 2))"/>
    <xsl:variable name="bDay" select="number(substring($beginDate, 9, 2))"/>
    <xsl:variable name="eYear" select="number(substring($endDate, 1, 4))"/>
    <xsl:variable name="eMonth" select="number(substring($endDate, 6, 2))"/>
    <xsl:variable name="eDay" select="number(substring($endDate, 9, 2))"/>

    <xsl:choose>
      <xsl:when test="$bYear != $eYear">
        <xsl:value-of select="$bDay"/>
        <xsl:text> </xsl:text>
        <xsl:call-template name="printMonthName">
          <xsl:with-param name="month" select="$bMonth"/>
        </xsl:call-template>
        <xsl:text> </xsl:text>
        <xsl:value-of select="$bYear"/>
        <xsl:text> - </xsl:text>
      </xsl:when>

      <xsl:when test="$bMonth != $eMonth">
        <xsl:value-of select="$bDay"/>
        <xsl:text> </xsl:text>
        <xsl:call-template name="printMonthName">
          <xsl:with-param name="month" select="$bMonth"/>
        </xsl:call-template>
        <xsl:text> - </xsl:text>
      </xsl:when>

      <xsl:when test="$bDay != $eDay">
        <xsl:value-of select="$bDay"/>
        <xsl:text> - </xsl:text>
      </xsl:when>
    </xsl:choose>

    <xsl:value-of select="$eDay"/>
    <xsl:text> </xsl:text>
    <xsl:call-template name="printMonthName">
      <xsl:with-param name="month" select="$eMonth"/>
    </xsl:call-template>
    <xsl:text> </xsl:text>
    <xsl:value-of select="$eYear"/>
  </xsl:template>




  <!-- Print the full month name to the given month number.
    1 = January
    2 = February
    ...
    12 = December
  -->
  <xsl:template name="printMonthName">
    <xsl:param name="month"/>

    <xsl:choose>
      <xsl:when test="$month = 1">Januar</xsl:when>
      <xsl:when test="$month = 2">February</xsl:when>
      <xsl:when test="$month = 3">March</xsl:when>
      <xsl:when test="$month = 4">April</xsl:when>
      <xsl:when test="$month = 5">May</xsl:when>
      <xsl:when test="$month = 6">June</xsl:when>
      <xsl:when test="$month = 7">July</xsl:when>
      <xsl:when test="$month = 8">August</xsl:when>
      <xsl:when test="$month = 9">September</xsl:when>
      <xsl:when test="$month = 10">Oktober</xsl:when>
      <xsl:when test="$month = 11">November</xsl:when>
      <xsl:when test="$month = 12">December</xsl:when>
    </xsl:choose>
  </xsl:template>




  <!-- Print the short three-letter month name to the given month number.
    1 = JAN
    2 = FEB
    ...
    12 = DEC
  -->
  <xsl:template name="printMonthShortName">
    <xsl:param name="month"/>

    <xsl:choose>
      <xsl:when test="$month = 1">JAN</xsl:when>
      <xsl:when test="$month = 2">FEB</xsl:when>
      <xsl:when test="$month = 3">MAR</xsl:when>
      <xsl:when test="$month = 4">APR</xsl:when>
      <xsl:when test="$month = 5">MAY</xsl:when>
      <xsl:when test="$month = 6">JUN</xsl:when>
      <xsl:when test="$month = 7">JUL</xsl:when>
      <xsl:when test="$month = 8">AUG</xsl:when>
      <xsl:when test="$month = 9">SEP</xsl:when>
      <xsl:when test="$month = 10">OKT</xsl:when>
      <xsl:when test="$month = 11">NOV</xsl:when>
      <xsl:when test="$month = 12">DEC</xsl:when>
    </xsl:choose>
  </xsl:template>




  <!-- Template for extracting the file ending from a path string.
    This template extracts the substring after the last dot in the path.

    Parameter:
     * path: the path to extract the file ening from
  -->
  <xsl:template name="extractFileEnding">
    <xsl:param name="path" />

    <xsl:choose>
      <xsl:when test="contains($path, '.')">
        <xsl:call-template name="extractFileEnding">
          <xsl:with-param name="path" select="substring-after($path, '.')" />
        </xsl:call-template>
      </xsl:when>

      <!-- Check if the path ends with a dot, or has zero length -->
      <xsl:when test="string-length($path) = 0">
        <xsl:value-of select="'file'" />
      </xsl:when>

      <xsl:otherwise>
        <xsl:value-of select="$path" />
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>




  <!-- Template for extracting the file name from a path string.
    This template extracts the substring after the last slash in the path.

    Parameter:
     * path: the path to extract the file name from
  -->
  <xsl:template name="extractFileName">
    <xsl:param name="path" />

    <xsl:choose>
      <xsl:when test="contains($path, '/')">
        <xsl:call-template name="extractFileName">
          <xsl:with-param name="path" select="substring-after($path, '/')" />
        </xsl:call-template>
      </xsl:when>

      <!-- Check if the path ends with a dot, or has zero length -->
      <xsl:when test="string-length($path) = 0">
        <xsl:value-of select="'file'" />
      </xsl:when>

      <xsl:otherwise>
        <xsl:value-of select="$path" />
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>



</xsl:stylesheet>
